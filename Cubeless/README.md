# Teoretický úvod
Tato práce popisuje algoritmus a implementaci detekci hran metodou první derivace. 
		
Pro demonstraci principu lze použít případ jednorozměrného signálu  na obr.
![1D hrana](/1d.png)
 Hranu(míru gradientu) získáme pomocí onvoluce vstupního signálu a jádra [1 -1]. Ve své podstatě jde o rozdíl vzorku x[n] a x[n-1]. Z tohoto důvodu není možné zpracovávat krajní body signálu (polovinu délky jádra). V případě druhé derivace můžeme získat i informací o směru hrany, samotná hrana se určí jako průsečík druhé derivace a osy X.

Pro 2D signál je princip obdobný, s rozdílem spočívajícím v přiřazení větší váhy prostřednímu pixelu, což má za následek lepší lokalizaci hran.
Samotná detekce je provedená v následujících krocích:

*	Rozostření - hranová detekce je velmi náchylná na šum. proto je vhodné provést rozostření, například gaussovským filtrem. Provádí se konvolucí s vhodným kernelem. Pro syntetické obrazy nezatížené šumem tento krok není zapotřebí.

*	Extrakce hran pro horizontální a vertikální směr pomocí odpovídájících konvolučních kernelů.

*	Stanovení velikosti hrany pomocí euklidovské vzdálenosti mezi velikostmi mezi horizontálním a vertikálním směrem a následné prahování. Reálně se jde vyhnout použití odmocniny a počítat průměr nebo maximum.

*	Další kroky mohou obsahovat určení směru hrany a prahování na více úrovní, následované hysterezi.

# Implementace
Pro vykresleni obrazu na LCD je funkce 
```C 
void imshow(uint8_t (*im)[240])
```
Jako parametr je ukazatel na 2D pole rozměru 240 x 240. Z důvodu úspory paměti a faktu že hranový detektor pracuje se stupni šedi se nepředpokládá barevný výstup. Samotná funkce upraví data a nakopíruje je do externí RAM, která slouží jako framebuffer. Data jsou ukládána ve formátu ARGB8888, kde A je Alpha kanál určující průhlednost pixelu, zbytek jsou barevné složky.

Ekvivalentní funkcí je funkce 
```C
  void iconv2d_fast(uint8_t (*src)[240], uint8_t (*dst)[240], int16_t (*kernel)[3] )
```
která používá intrinzické funkce pro využítí SIMD instrukcí. Konkrétně se jedná o funkci 
```C
  unsigned int __smlad(unsigned int val1, unsigned int val2, unsigned int val3)
```
kde se nezávisle vynásobí horní a dolní poloviny val1 a val2, následně se výsledek přičte k hodnotě val3. Teoreticky jde tedy o trojnásobné zrychlení oprotí konvenčním postupu, ale díky nutnosti připravovat data (bitové posuvy) je čas běhu funkcí prakticky stejný. Dále tento fakt podporuje skutečnost, že se vždy pracuje v 2D okolí nějakého bodu, tedy část výkonu jde na adresování.

Hlavní program se vykonává ve funkci *gui()*, která obsahuje atribut *noreturn*. To je z důvodu, že funkce main je generována automaticky systémem STM32Cube. Všechny funkce zde popsány jsou v souboru graphics.c/.h.  Samotný obrázek je uložen jako 2D pole a je definován v images.h. Dále součásti projektu je matlabovský skript pro převod obrázku do formátu vhodného pro překladač.

# Závěr

[Video ukázka](https://youtu.be/Rds0vfH_BHY)

Cílem tohoto projektu bylo implementovat algoritmus detekce hran. Nebyla implementovana funkčnost načitání obrázku z flash disku, je pouze vytvořen matlabovský skript pro převod obrázku do formátu C pole.
Byly vytvořeny dvě implementace 2D konvoluce, jedná s pomocí intrinzických funkcí. Rozdíl v rychlosti je však díky větší režii nepatrný. Daleko větší vliv měla optimalizace překladače, kde došlo ke zrychlení z 24ms na 12ms.  
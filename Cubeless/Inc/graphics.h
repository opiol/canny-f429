#ifndef GRAPHICS_H_
#define GRAPHICS_H_

/**
 * @brief graficke operace
 * @file graphics.h
 * @author Zbigniew Opiol <xopiol00@stud.feec.vutbr.cz>
 */

#include <math.h>
#include "stm32f429i_discovery_lcd.h"
#include <stdint.h>

#define ANGLE_UI8(degrees)	((uint8_t)(255.0*degrees/360)) /**< prevod float uhlu 0..360 na uint8_t*/
/**
 * @brief vykresli cernobile na LCD
 * @param[in] ukazatel na pole 240x240 
 **/
void imshow(uint8_t (*im)[240]);
/**
 * @brief
 * @param input 
 **/
void imshow_bin(uint8_t (*im)[240]);
/**
 * @brief
 * @param input 
 **/
void binarize(uint8_t (*im)[240]);
/**
 * @brief
 * @param input 
 **/
void fconv2d(uint8_t (*src)[240], uint8_t (*dst)[240], float (*kernel)[3] );
/**
 * @brief
 * @param input 
 **/
void iconv2d(uint8_t (*src)[240], uint8_t (*dst)[240], int16_t (*kernel)[3] );
/**
 * @brief
 * @param input 
 **/
void iconv2d_fast(uint8_t (*src)[240], uint8_t (*dst)[240], int16_t (*kernel)[3] );
/**
 * @brief
 * @param input 
 **/
void fmagn_angle(uint8_t (*op1)[240], uint8_t (*op2)[240]);
/**
 * @brief
 * @param input 
 **/
void suppress(uint8_t (*op1)[240], uint8_t (*op2)[240]);

#endif /* GRAPHICS_H_ */

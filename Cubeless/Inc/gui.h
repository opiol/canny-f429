#ifndef GUI_H_
#define GUI_H_

/**
 * @brief hlaviskovy soubor pro utek z generovaneho kodu
 * @file gui.h
 * @author Zbigniew Opiol <xopiol00@stud.feec.vutbr.cz>
 */


#include <stdint.h>
/**
 * @brief nekonecna funkce misto mainu
 * @return never returns
 */
void gui()	__attribute__((noreturn));

//void imshow(uint8_t *** im);

#endif /* GUI_H_ */

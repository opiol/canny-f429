/*
 * graphics.c
 *
 *  Created on: Dec 18, 2019
 *      Author: opiol
 */

#include"graphics.h"
#include "arm_math.h"

void imshow(uint8_t (*im)[240])
{
	uint16_t x,y;
	volatile uint32_t lcd_pixel;

	for(x = 0; x < 240; x++)
		for(y = 0; y<240;y++)
		{
			lcd_pixel = (((uint32_t)(im[x][y])) << 16)  | (((uint32_t)(im[x][y])) << 8) | (((uint32_t)(im[x][y])));
			lcd_pixel |= 0xff000000;
			BSP_LCD_DrawPixel(x, y, lcd_pixel);
		}
}

void imshow_bin(uint8_t (*im)[240])
{
	uint16_t x,y;
	for(x = 0; x < 240; x++)
		for(y = 0; y<240;y++)
			BSP_LCD_DrawPixel(x, y, im[x][y] ? 0xffffffff: 0x00000000);
}

void binarize(uint8_t (*im)[240])
{
	uint16_t x,y;
	for(x = 0; x < 240; x++)
		for(y = 0; y<240;y++)
			im[x][y] =  im[x][y] ? 255: 0 ;
}

void fconv2d(uint8_t (*src)[240], uint8_t (*dst)[240], float (*kernel)[3] )
{
	uint16_t x,y;
	float acc = 0;

	for(y = 1; y < (240 -1); y++)
		for(x = 1; x < 240 -1; x++)
		{
			acc = 	kernel[0][0] * (float)src[y-1][x-1] + kernel[0][1]*(float)src[y-1][x] + kernel[0][2]*(float)src[y-1][x+1] ;
			acc += 	kernel[1][0] * (float)src[y ][x-1]  + kernel[1][1]*(float)src[y][x]   + kernel[1][2]*(float)src[y  ][x+1] ;
			acc +=	kernel[2][0] * (float)src[y+1][x-1] + kernel[2][1]*(float)src[y+1][x] + kernel[2][2]*(float)src[y+1][x+1] ;
			dst[y][x] = (uint8_t)acc;
		}
}

void iconv2d(uint8_t (*src)[240], uint8_t (*dst)[240], int16_t (*kernel)[3] )
{
	uint16_t x,y;
	int32_t acc = 0;

	for(y = 1; y < (240 -1); y++)
		for(x = 1; x < 240 -1; x++)
		{
			acc = 	kernel[0][0] * (int16_t)src[y-1][x-1] + kernel[0][1]*(int16_t)src[y-1][x] + kernel[0][2]*(int16_t)src[y-1][x+1] ;
			acc += 	kernel[1][0] * (int16_t)src[y ][x-1]  + kernel[1][1]*(int16_t)src[y][x]   + kernel[1][2]*(int16_t)src[y  ][x+1] ;
			acc +=	kernel[2][0] * (int16_t)src[y+1][x-1] + kernel[2][1]*(int16_t)src[y+1][x] + kernel[2][2]*(int16_t)src[y+1][x+1] ;
			dst[y][x] = (uint8_t)acc;
		}
}


void iconv2d_fast(uint8_t (*src)[240], uint8_t (*dst)[240], int16_t (*kernel)[3] )
{
	uint16_t x,y;
	int32_t acc = 0,op1,op2;

	for(y = 1; y < (240 -1); y++)
		for(x = 1; x < 240 -1; x++)
		{
			acc=0;
			acc = __SMLAD(kernel[0][0] << 16 | kernel[0][1], src[y-1][x-1] << 16 | src[y-1][x], acc);
			acc = __SMLAD(kernel[0][2] << 16 | kernel[1][0], src[y-1][x+1] << 16 | src[y][x-1], acc);
			acc = __SMLAD(kernel[1][1] << 16 | kernel[1][2], src[y][x]     << 16 | src[y][x+1], acc);
			acc = __SMLAD(kernel[2][0] << 16 | kernel[2][1], src[y+1][x-1] << 16 | src[y+1][x], acc);
			acc = __SMLAD(0					 | kernel[2][2], 0			   << 16 | src[y+1][x+1], acc);
			dst[y][x] = (uint8_t)acc;
		}
}

/*
 * op1 : Sobel Y
 * op2 ; Sobel X
 *
 *retval op1: Magnitude
 *retval op2: normalized angle
 */
void fmagn_angle(uint8_t (*op1)[240], uint8_t (*op2)[240])
{
	uint16_t x,y;
	float tmp = 0;
	uint16_t a,b;

	for(y = 1; y < (240 -1); y++)
		for(x = 1; x < 240 -1; x++)
		{
			a = op1[y][x];
			b = op2[y][x];
			op1[y][x] = (uint8_t)((a+b)>> 1);

			tmp = atanf( a /b );
			tmp += M_PI/2;
			tmp = tmp * (255/M_PI);
			op2[y][x] = (uint8_t)tmp;
		}
}

void suppress(uint8_t (*op1)[240], uint8_t (*op2)[240])
{
	uint16_t x,y,q,r;

	for(y = 1; y < (240 -1); y++)
		for(x = 1; x < 240 -1; x++)
		{
			q = 255;
			r = 255;

			if( (ANGLE_UI8(0) <= op1[y][x] < ANGLE_UI8(22.5)) | (ANGLE_UI8(157.5) <= op1[y][x] <= ANGLE_UI8(180)) ){
				q = op2[y][x+1];
				r = op2[y][x-1];
			}
			else if( ANGLE_UI8(22.5) <= op1[y][x] < ANGLE_UI8(67.5) ){
				q = op2[y+1][x-1];
				r = op2[y-1][x+1];
			}
			else if( ANGLE_UI8(67.5) <= op1[y][x] < ANGLE_UI8(112.5) ){
				q = op2[y+1][x];
				r = op2[y-1][x];
			}
			else if( ANGLE_UI8(112.5) <= op1[y][x] < ANGLE_UI8(157.5) ){
				q = op2[y-1][x-1];
				r = op2[y+1][x+1];
			};

			if(( op2[y][x] >= q ) && (op2[y][x] >= r))
				op2[y][x] = 255;
			else
				op2[y][x] = 0;
		}
}

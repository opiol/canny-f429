#include <stdio.h>

#include "stm32f429i_discovery_sdram.h"
#include "stm32f429i_discovery_lcd.h"
#include "stm32f429i_discovery_ts.h"

#include "gui.h"
#include "images.h"
#include "graphics.h"

TS_StateTypeDef pen;
uint8_t img[240][240] = {0};
uint8_t A[240][240] /*__attribute__ ((section (".edata"))) */;
uint8_t B[240][240] = {0};

float blur[3][3] = {
		{1.0/16, 1.0/8, 1.0/16},
		{1.0/8,  1.0/4, 1.0/8 },
		{1.0/16, 1.0/8, 1.0/16}
};

int16_t sobelx[3][3] ={
		{-1, 0, 1},
		{-2, 0, 2},
		{-1, 0, 1}
};

int16_t sobely[3][3] ={
		{1, 2, 1},
		{0, 0, 0},
		{-1, -2, -1}
};

void init()
{
	BSP_LED_Init(LED3);
	BSP_PB_Init(BUTTON_KEY, BUTTON_MODE_GPIO);
	BSP_TS_Init(240, 320);
//	BSP_TS_ITConfig();
	BSP_LCD_Init();		//init LCD
	BSP_LCD_LayerDefaultInit(1, SDRAM_DEVICE_ADDR);
	BSP_LCD_SelectLayer(1);
	BSP_LCD_DisplayOn();
	BSP_LCD_Clear(LCD_COLOR_BLACK);
	BSP_LCD_SetBackColor(LCD_COLOR_BLACK);
	BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
	stmpe811_SetITType(TS_I2C_ADDRESS, STMPE811_TYPE_EDGE);
}

void getkey(void)
{
	while(!BSP_PB_GetState(BUTTON_KEY)) {HAL_Delay(10);}
	while(BSP_PB_GetState(BUTTON_KEY)) {HAL_Delay(10);}
}

void gui()
{
	uint8_t (*img)[240] = SHAPES;
	uint32_t i,t;
	char str[16] = {0};

	init();
	NVIC_DisableIRQ(EXTI15_10_IRQn);

	imshow(img);

	getkey();
	BSP_LCD_Clear(LCD_COLOR_BLACK);
	t = HAL_GetTick();
	for(i = 0; i < 16; i++)
		iconv2d_fast(img, A, sobely);
	imshow(A);
	BSP_LCD_DisplayStringAtLine(10, "sobel Y");
	sprintf(str,"%d ms",(HAL_GetTick() - t) >> 4);
	BSP_LCD_DisplayStringAtLine(11, str);


	getkey();
	t = HAL_GetTick();
	for(i = 0; i < 16; i++)
		iconv2d(img, B, sobelx);
	sprintf(str,"%d ms",(HAL_GetTick() - t) >> 4);
	BSP_LCD_Clear(LCD_COLOR_BLACK);
	BSP_LCD_DisplayStringAtLine(10, "sobel X");
	BSP_LCD_DisplayStringAtLine(11, str);
	imshow(B);

	getkey();
	fmagn_angle(A, B);
	BSP_LCD_Clear(LCD_COLOR_BLACK);
	BSP_LCD_DisplayStringAtLine(10, "Edges");
	binarize(A);
	imshow(A);

	getkey();

	/* no return */
	while(1);
}


void EXTI15_10_IRQHandler(void)
{
	//stmpe811_ClearGlobalIT(TS_I2C_ADDRESS, STMPE811_ALL_GIT);
	//	NVIC_DisableIRQ(EXTI15_10_IRQn);
}

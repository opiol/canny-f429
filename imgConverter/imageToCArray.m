%% Image to C Array Conversion %%
% This program converts a BMP/TIFF/JPG/PNG file to an embedded C/C++ byte array. This is very helpful when you want to display a monochrome image on your display.
% It converts your image monochrome before data array generation. The darker colors in the source image will produce 'on' pixels, while the lighter colors will be 'off' pixels.
% The code provides an intermediate output of the created monochrome image for your validation.
% The code also resizes the input image based on the width & height entered before array generation.
% Copyright 2019 The MathWorks, Inc.
function imageToCArray
%% Specify the required width & height of the image based on the display
prompt = {'Enter required width (in pixels)','Enter required height (in pixels)'};
dlgtitle = 'Enter width & height for image';
dims = [1 70];
definput = {'200','200'};
whVal = inputdlg(prompt,dlgtitle,dims,definput);
if(isempty(whVal)), return; end
width=str2double(whVal(1));
height=str2double(whVal(2));
%% Check if entered value is numeric, else stop the program with error 
if(isnan(width) || isnan(height))
    warndlg('Please enter a valid number to proceed.');
    return;
end
%% Read the image from the file
[filename, pathname] = uigetfile('*.bmp;*.tiff;*.jpg;*.png','Choose an image for conversion');
selectedfile =fullfile(pathname,filename);
%[filepath,name,ext] = fileparts(selectedfile);
if ~isfile(selectedfile)
    return;
end
try
    img = imread(selectedfile);
    [icondata,iconcmap] = imread(selectedfile); 
catch
    errorMessage = sprintf('Error: Invalid image file chosen');
    uiwait(warndlg(errorMessage));
    return;
end
%% Resize & convert image to binary
img = imresize(img,[width height]);
[ ~, ~ , c ] = size(img);
if c == 3
    img = rgb2gray(img);
    level = graythresh(img);
    %img = imbinarize(img,level);
    figure('Name',filename);imshow(img);
else
    img=uint8(img);
    level = graythresh(img);
    img = imbinarize(img,level);
    figure('Name',filename);imshow(img);
end
img=img';
%% Check with user if the image output is satisfactory
quesAns = questdlg('Proceed to Array generation ?', ...
    'Check image before proceeding', ...
    'Yes, Proceed','No, Cancel','No, Cancel');
switch quesAns
    case 'No, Cancel'
        close all;
        return;
end
%% C array output file generation (File generated in present working directory)
textFile = fopen('imagedata.cpp','wt');
fprintf(textFile,['#include "imagedata.h"\n\n']); % Change based on controller requirements
fprintf(textFile,['// File: ' filename ', Size: ' num2str(width) ' x ' num2str(height) '\n']);
fprintf(textFile,['const unsigned char IMAGE_DATA[] PROGMEM = { \n']); % Change based on controller requirements
wrap=0; % for formatting
% Horizontal Scanning
for w=1:8:numel(img)
    x=1:8:numel(img);y=x(end); % for checking last compute
    wrap=wrap+1;
    if w==y
        eq = img(w:w+(numel(img)-y)); % Checking for sizes not divisible by 8
    else
        eq = img(w:w+7);
    end
    val = dec2hex(((eq)));
    if length(val)==1 % formatting
        if w==y
            fprintf(textFile,['0x0' val]); % removing ',' from last compute
        else
            fprintf(textFile,['0x0' val ',']);
        end
    else
        if w==y
            fprintf(textFile,['0x' val]); % removing ',' from last compute
        else
            fprintf(textFile,['0x' val ' ,']);
        end
    end
    if mod(wrap,16)==0 % formatting to wrap to the next line
        fprintf(textFile,'\n');
    end
end
fprintf(textFile,'};\n');
fclose(textFile);
%% Generation complete
msgbox('Array generated, open ''imagedata.cpp'' to verify','Array Generated','custom',uint8(icondata),iconcmap);
clear;
close gcf; % current figure handle
end
clc
close all 
clear all

filename = 'shapes.png';

width = 240-1;
height = 240-1;

textFile = fopen('img.c','wt');
fprintf(textFile,['// File: ' filename ', Size: ' num2str(width+1) ' x ' num2str(height+1) '\n']);
fprintf(textFile,'const uint8_t SHAPES[][]  = { \n'); % Change based on controller requirements

f = (imread(filename));
f = imresize(f,[height,width]);

z = zeros(width,height);

for i=1:width
    
    fprintf(textFile,'{');
    
    for j = 1:height     
        val = dec2hex(f(j,i),2);
        z(j,i) = f(j,i); 
         fprintf(textFile,'0x');
         fprintf(textFile,val);
         fprintf(textFile, ', ' ); 
         
         if height == j 
            fprintf(textFile,'0x');
            fprintf(textFile, val ); 
            fprintf(textFile, '},\n' ); 
         end
              
    end   
end

fprintf(textFile,'};\n');
fclose(textFile);
imshow(z,[]);